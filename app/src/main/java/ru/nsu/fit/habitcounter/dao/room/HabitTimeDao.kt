package ru.nsu.fit.habitcounter.dao.room

import androidx.room.*
import ru.nsu.fit.habitcounter.model.dto.HabitTime
import java.util.*

@Dao
interface HabitTimeDao {
    @Query("SELECT * FROM HabitTime")
    fun getAll(): List<HabitTime>

    @Query("SELECT * FROM HabitTime WHERE habitId= :habitId")
    fun getByHabitId(habitId: Long): List<HabitTime>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg habit: HabitTime)

    @Update
    fun update(vararg habit: HabitTime)

    @Delete
    fun delete(vararg habit: HabitTime)
}
