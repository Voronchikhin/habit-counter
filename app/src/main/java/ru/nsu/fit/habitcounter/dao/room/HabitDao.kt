package ru.nsu.fit.habitcounter.dao.room

import androidx.room.*
import ru.nsu.fit.habitcounter.model.dto.Habit


@Dao
interface HabitDao {
    @Query("SELECT * FROM HABIT")
    fun getAll(): List<Habit>

    @Query("SELECT * FROM HABIT WHERE name= :name")
    fun getByName(name: String): Habit

    @Query("SELECT * FROM HABIT WHERE id= :id")
    fun getById(id: Long): Habit

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg habit: Habit)

    @Update
    fun update(vararg habit: Habit)

    @Delete
    fun delete(vararg habit: Habit)
}