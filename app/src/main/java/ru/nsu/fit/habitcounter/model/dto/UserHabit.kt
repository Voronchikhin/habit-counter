package ru.nsu.fit.habitcounter.model.dto

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(primaryKeys = ["userId", "habitId"], foreignKeys = [
    ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["userId"]),
    ForeignKey(entity = Habit::class, parentColumns = ["id"], childColumns = ["habitId"])
])
data class UserHabit(
        val userId: Long,
        val habitId: Long
)