package ru.nsu.fit.habitcounter.dao.room

import androidx.room.*
import ru.nsu.fit.habitcounter.model.dto.ResourceMeasure

@Dao
interface ResourceMeasureDao {
    @Query("SELECT * FROM ResourceMeasure")
    fun getAll(): List<ResourceMeasure>

    @Query("SELECT * FROM ResourceMeasure WHERE resourceId = :resourceId")
    fun getByResourceId(resourceId: Long): List<ResourceMeasure>

    @Query("SELECT * FROM ResourceMeasure WHERE measureId = :measureId")
    fun getByMeasureId(measureId: Long): List<ResourceMeasure>

    @Insert
    fun insert(vararg resourceMeasure: ResourceMeasure)

    @Update
    fun update(vararg resourceMeasure: ResourceMeasure)

    @Delete
    fun delete(vararg resourceMeasure: ResourceMeasure)
}