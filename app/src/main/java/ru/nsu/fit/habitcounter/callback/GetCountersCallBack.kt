package ru.nsu.fit.habitcounter.callback

import ru.nsu.fit.habitcounter.model.HabitCounter

interface GetCountersCallBack {
    fun onCountersLoaded(counters: Collection<HabitCounter>)
    fun onError()
}