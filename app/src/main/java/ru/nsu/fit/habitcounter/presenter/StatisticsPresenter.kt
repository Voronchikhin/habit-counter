package ru.nsu.fit.habitcounter.presenter

import ru.nsu.fit.habitcounter.presenter.common.Presenter
import ru.nsu.fit.habitcounter.presenter.common.UseCaseHandler
import ru.nsu.fit.habitcounter.presenter.common.View

class StatisticsPresenter(
    private val view: View<StatisticsPresenter>,
    private val useCaseHandler: UseCaseHandler
) :
    Presenter {
    private fun loadStatistics() {

    }

    override fun start() {
        view.presenter = this
    }
}