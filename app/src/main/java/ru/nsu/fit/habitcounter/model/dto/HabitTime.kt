package ru.nsu.fit.habitcounter.model.dto

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.time.LocalDateTime
import java.util.*

@Entity(
    foreignKeys = [
        ForeignKey(entity = Habit::class, parentColumns = ["id"], childColumns = ["habitId"])
    ]
)
class HabitTime(
    @PrimaryKey(autoGenerate = true)
    var habitTimeId: Long? = null,
    var habitId: Long = 0,
    var time: LocalDateTime? = null
)

