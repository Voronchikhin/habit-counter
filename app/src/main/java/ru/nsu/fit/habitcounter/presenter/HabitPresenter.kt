package ru.nsu.fit.habitcounter.presenter

import ru.nsu.fit.habitcounter.dao.HabitCounterRepository
import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.callback.CallBack
import ru.nsu.fit.habitcounter.presenter.common.Presenter
import ru.nsu.fit.habitcounter.presenter.common.UseCaseHandler
import ru.nsu.fit.habitcounter.presenter.common.View
import ru.nsu.fit.habitcounter.view.IHabitView

class HabitPresenter(
    private val view: View<HabitPresenter>,
    private val useCaseHandler: UseCaseHandler,
    repository: HabitCounterRepository
) :
    Presenter {
    private fun loadHabits() {
        useCaseHandler.execute(loadUseCase, HabitRequest(), object :
            CallBack<HabitResponse> {
            override fun onSuccess(response: HabitResponse) {
                val habits = response.list
                (view as IHabitView).showHabits(habits)
            }

            override fun onError() {
                (view as IHabitView).showError("Loading habits error")
            }
        })
    }

    private val loadUseCase = LoadHabitCountersUseCase(
        repository
    )
    private val countUseCase = DoCountUseCase(
        repository
    )
    private val getStatisticsUseCase = GetStatisticsUseCase(
        repository
    )


    override fun start() {
        view.presenter = this
        loadHabits()
    }

    fun doCount(habitCounter: HabitCounter) {
        useCaseHandler.execute(countUseCase, DoCountRequest(habitCounter), object :
            CallBack<DoCountResponse> {
            override fun onError() {
                (view as IHabitView).showError("Counting error")
            }

            override fun onSuccess(response: DoCountResponse) {
                (view as IHabitView).updateHabit(response.habitCounter)
            }
        })
    }

    fun showStatistics(habitCounter: HabitCounter) {
        useCaseHandler.execute(getStatisticsUseCase, GetStatisticsRequest(habitCounter), object :
            CallBack<GetStatisticsResponse> {
            override fun onError() {
                (view as IHabitView).showError("Statistics error")
            }

            override fun onSuccess(response: GetStatisticsResponse) {
                (view as IHabitView).showStatistics(response.habitTimes)
            }
        })
    }
}