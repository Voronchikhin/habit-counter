package ru.nsu.fit.habitcounter.callback

import ru.nsu.fit.habitcounter.presenter.common.UseCase

interface CallBack<Response : UseCase.ResponseValue> {
    fun onSuccess(response: Response)
    fun onError()
}