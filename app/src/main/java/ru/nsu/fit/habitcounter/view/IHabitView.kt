package ru.nsu.fit.habitcounter.view

import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.model.dto.HabitTime
import ru.nsu.fit.habitcounter.presenter.HabitPresenter
import ru.nsu.fit.habitcounter.presenter.common.View

interface IHabitView : View<HabitPresenter> {
    fun showHabits(habits: Collection<HabitCounter>)
    fun updateHabit(habitCounter: HabitCounter)
    fun showStatistics(habitTimes: List<HabitTime>)
    fun showError(message: String)
}