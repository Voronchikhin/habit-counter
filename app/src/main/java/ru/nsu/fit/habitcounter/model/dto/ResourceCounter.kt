package ru.nsu.fit.habitcounter.model.dto

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [
        ForeignKey(entity = Resource::class, parentColumns = ["id"], childColumns = ["resourceId"]),
        ForeignKey(entity = Habit::class, parentColumns = ["id"], childColumns = ["habitId"])
    ]
)
data class ResourceCounter (
    val habitId: Long,
    val resourceId: Long,
    var count: Long,
    val increment: Long
){
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
}
