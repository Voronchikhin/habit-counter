package ru.nsu.fit.habitcounter.model.dto

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(primaryKeys = ["resourceId", "measureId"], foreignKeys = [
    ForeignKey(entity = Resource::class, parentColumns = ["id"], childColumns = ["resourceId"]),
    ForeignKey(entity = Measure::class, parentColumns = ["id"], childColumns = ["measureId"])
])
data class ResourceMeasure(
        val resourceId: Long,
        val measureId: Long
)