package ru.nsu.fit.habitcounter.callback

import ru.nsu.fit.habitcounter.model.HabitCounter

interface UpdateHabitCallBack {
    fun onAddHabit(addedCounter: HabitCounter)
    fun onError()
}