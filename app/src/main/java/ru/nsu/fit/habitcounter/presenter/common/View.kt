package ru.nsu.fit.habitcounter.presenter.common

interface View<T: Presenter> {
    var presenter: Presenter
}