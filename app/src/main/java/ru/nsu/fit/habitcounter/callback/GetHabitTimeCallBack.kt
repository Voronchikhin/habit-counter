package ru.nsu.fit.habitcounter.callback

import ru.nsu.fit.habitcounter.model.dto.HabitTime

interface GetHabitTimeCallBack {
    fun onGetHabitTimes(habitTimes: List<HabitTime>)
    fun onError()
}