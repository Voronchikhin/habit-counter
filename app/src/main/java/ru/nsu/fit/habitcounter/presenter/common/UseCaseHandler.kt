package ru.nsu.fit.habitcounter.presenter.common

import ru.nsu.fit.habitcounter.callback.CallBack
import ru.nsu.fit.habitcounter.callback.UseCaseCallBackCallWrapper


class UseCaseHandler {
    private val scheduler = UseCaseSchedulerImpl

    fun <ResponseType : UseCase.ResponseValue, RequestType : UseCase.RequestValue> execute(
        useCase: UseCase<ResponseType, RequestType>,
        request: RequestType,
        callBack: CallBack<ResponseType>
    ) {
        useCase.requestValue = request
        useCase.callBack =
            UseCaseCallBackCallWrapper(callBack, this)
        scheduler.threadPoolExecutor.submit { useCase.run() }
    }

    fun <ResponseType : UseCase.ResponseValue> notifyOnSuccess(
        response: ResponseType,
        callBack: CallBack<ResponseType>
    ) {
        scheduler.onResponse(response, callBack)
    }

    fun <ResponseType : UseCase.ResponseValue> notifyOnError(callBack: CallBack<ResponseType>) {
        scheduler.onError(callBack)
    }
}
