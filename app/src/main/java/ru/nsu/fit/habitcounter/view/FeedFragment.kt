package ru.nsu.fit.habitcounter.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.model.dto.HabitTime
import ru.nsu.fit.habitcounter.presenter.HabitPresenter
import ru.nsu.fit.habitcounter.presenter.common.Presenter
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle


class FeedFragment : Fragment(), IHabitView {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun showStatistics(habitTimes: List<HabitTime>) {
        val intent = Intent(this.activity, StatisticsActivity::class.java)
        intent.putExtra(
            "TimeList",
            habitTimes
                .map {
                    it.time?.format(
                        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG).withZone(ZoneId.systemDefault())
                    )
                }
                .fold("") { it, next ->
                    it + "\n" + next
                }
        )
        startActivity(intent)
    }

    override fun updateHabit(habitCounter: HabitCounter) {
        adapter.notifyDataSetChanged()
    }


    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG)
    }

    override fun showHabits(habits: Collection<HabitCounter>) {
        adapter = HabitAdapter(habits.toList())
        recyclerView.adapter = adapter
    }


    lateinit var recyclerView: RecyclerView
    lateinit var adapter: HabitAdapter

    override lateinit var presenter: Presenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_feed, container, false)
        recyclerView = view.findViewById(R.id.feed_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = HabitAdapter(emptyList())
        return view
    }

    inner class HabitHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.feed_item, parent, false)) {
        var habitName: TextView = itemView.findViewById(R.id.feed_habit_name)
        var habitCount: TextView = itemView.findViewById(R.id.feed_habit_count)
        val countButton: Button = itemView.findViewById(R.id.doCountButton)
        val statButton: Button = itemView.findViewById(R.id.showStatisticsButton)

        lateinit var counter: HabitCounter
        fun bind(habitCounter: HabitCounter) {
            this.counter = habitCounter
            habitName.text = this.counter.habit.name
            this.habitCount.text = this.counter
                .resourceCounters
                .joinToString(
                    separator = "\n",
                    transform = { "${it.resource.name} : ${it.resourceCounter.count}\n" }
                )
            val image: ImageView =
                itemView.findViewById(R.id.feed_habit_view)

            val smokeLogo = if (this.counter.habit.name == "Smoke") {
                R.drawable.smoke_logo
            } else {
                R.drawable.coffee
            }
            image.setImageResource(smokeLogo)
            countButton.setOnClickListener {
                (presenter as HabitPresenter).doCount(habitCounter)
            }
            statButton.setOnClickListener {
                (presenter as HabitPresenter).showStatistics(habitCounter)
            }
        }

    }

    inner class HabitAdapter(var habits: List<HabitCounter>) : RecyclerView.Adapter<HabitHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): HabitHolder {
            val inflater: LayoutInflater = LayoutInflater.from(activity)
            return HabitHolder(inflater, parent)
        }

        override fun getItemCount(): Int = habits.size

        override fun onBindViewHolder(habitHolder: HabitHolder, idx: Int) {
            val habitCounter = habits.get(idx)
            habitHolder.bind(habitCounter)
        }
    }
}