package ru.nsu.fit.habitcounter.dao.room

import androidx.room.*
import ru.nsu.fit.habitcounter.model.dto.Log
import java.sql.Date

@Dao
interface LogDao {
    @Query("SELECT * FROM LOG")
    fun getAll(): List<Log>

    @Query("SELECT LOG.ID, USER.NAME, HABIT.NAME, LOG.TIME FROM LOG JOIN USER ON USER.ID=LOG.USERID JOIN HABIT ON HABIT.ID=LOG.HABITID")
    fun getFullAll(): List<Log>

    @Query("SELECT * FROM LOG WHERE id = :habitId")
    fun getByHabitId(habitId: Long): List<Log>

    @Query("SELECT * FROM LOG WHERE id = :userId")
    fun getByUserId(userId: Long): List<Log>

    @Query("SELECT * FROM LOG WHERE time BETWEEN :start AND :end")
    fun getByTimePeriod(start: Date, end: Date): List<Log>

    @Query("SELECT * FROM LOG WHERE time BETWEEN CURDATE() AND " +
            "DATE_SUB(CURDATE(), INTERVAL 30 DAY")
    fun getLast30Day(): List<Log>

    @Insert
    fun insert(vararg log: Log)

    @Update
    fun update(vararg log: Log)

    @Delete
    fun delete(vararg log: Log)

}