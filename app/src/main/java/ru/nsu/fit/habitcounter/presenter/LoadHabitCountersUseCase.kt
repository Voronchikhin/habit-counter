package ru.nsu.fit.habitcounter.presenter

import ru.nsu.fit.habitcounter.callback.GetCountersCallBack
import ru.nsu.fit.habitcounter.dao.HabitCounterRepository
import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.callback.CallBack
import ru.nsu.fit.habitcounter.presenter.common.UseCase

class LoadHabitCountersUseCase(private val habitCounterRepository: HabitCounterRepository) :
    UseCase<HabitResponse, HabitRequest>() {
    override lateinit var requestValue: HabitRequest
    override lateinit var callBack: CallBack<HabitResponse>

    override fun execute(x: HabitRequest){
        habitCounterRepository.getHabitCounters(object : GetCountersCallBack {
            override fun onCountersLoaded(counters: Collection<HabitCounter>) {
                callBack.onSuccess(HabitResponse(counters.toList()))
            }

            override fun onError() {
                callBack.onError()
            }
        })
    }
}

data class HabitResponse(val list: List<HabitCounter>) : UseCase.ResponseValue

class HabitRequest : UseCase.RequestValue