package ru.nsu.fit.habitcounter.model.dto

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(primaryKeys = ["habitId", "resourceId"], foreignKeys = [
    ForeignKey(entity = Habit::class, parentColumns = ["id"], childColumns =["habitId"] ),
    ForeignKey(entity = Resource::class, parentColumns = ["id"], childColumns =["resourceId"] )
])
data class HabitResource (
    val habitId: Long,
    val resourceId: Long
)
