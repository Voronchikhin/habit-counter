package ru.nsu.fit.habitcounter.presenter

import ru.nsu.fit.habitcounter.callback.UpdateHabitCallBack
import ru.nsu.fit.habitcounter.dao.HabitCounterRepository
import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.callback.CallBack
import ru.nsu.fit.habitcounter.presenter.common.UseCase

class DoCountUseCase(private val habitCounterRepository: HabitCounterRepository) : UseCase<DoCountResponse, DoCountRequest>() {
    override lateinit var callBack: CallBack<DoCountResponse>
    override lateinit var requestValue: DoCountRequest

    override fun execute(x: DoCountRequest) {
        val habitCounter = x.habitCounter
        habitCounter.doCount()
        habitCounterRepository.updateHabitCounter(habitCounter,object: UpdateHabitCallBack {
            override fun onAddHabit(addedCounter: HabitCounter) {
                callBack.onSuccess(DoCountResponse(addedCounter))
            }

            override fun onError() {
                callBack.onError()
            }
        })
        callBack.onSuccess(DoCountResponse(habitCounter))
    }
}

class DoCountResponse(val habitCounter: HabitCounter) : UseCase.ResponseValue

data class DoCountRequest(val habitCounter: HabitCounter) : UseCase.RequestValue