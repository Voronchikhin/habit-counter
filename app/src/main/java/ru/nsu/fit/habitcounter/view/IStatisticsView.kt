package ru.nsu.fit.habitcounter.view

import ru.nsu.fit.habitcounter.presenter.StatisticsPresenter
import ru.nsu.fit.habitcounter.presenter.common.View


interface IStatisticsView : View<StatisticsPresenter> {
    fun showStatistics()
    fun updateStatistics()
    fun showError()
}
