package ru.nsu.fit.habitcounter.dao

import ru.nsu.fit.habitcounter.model.dto.*

interface HabitDataSource {
    fun getHabits(): List<Habit>
    fun getResourceCounters(): List<ResourceCounter>
    fun getResources(): List<Resource>
    fun getHabitTimes(): List<HabitTime>
    fun getHabitResourceByHabitId(habitId: Long): List<HabitResource>
    fun getHabitTimeByHabitId(habitId: Long): List<HabitTime>

    fun getHabitResources(): List<HabitResource>
    fun getResourceById(resourceId: Long): Resource?
    fun getResourceCounterByResourceId(resourceId: Long, habitId: Long): ResourceCounter?


    fun updateResource(resource: Resource)
    fun updateHabit(habit: Habit)
    fun updateHabitResource(habitResource: HabitResource)
    fun updateResourceCounter(resourceCounter: ResourceCounter)

    fun insertHabitTime(habitTime: HabitTime)


    companion object {
        val MEMORY_DATA_SOURCE = object : HabitDataSource {

            override fun insertHabitTime(habitTime: HabitTime) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun updateResource(resource: Resource) {

            }

            override fun updateHabit(habit: Habit) {

            }

            override fun updateHabitResource(habitResource: HabitResource) {

            }

            override fun updateResourceCounter(resourceCounter: ResourceCounter) {

            }

            override fun getHabitTimes(): List<HabitTime> = habitTimeResource

            override fun getResourceCounters(): List<ResourceCounter> = resourceCounters

            override fun getResources(): List<Resource> = resources

            override fun getHabitResources(): List<HabitResource> = habitResources

            private val habits = listOf(
                Habit("Smoke", "Smoke cigarettes").apply { id = 1 },
                Habit("Coffee", "drink coffee").apply { id = 2 }
            )

            private val resources = listOf(
                Resource("Money", "Money spending").apply { id = 1 }
            )
            private val resourceCounters =
                resources.flatMap { resource ->
                    habits.map { ResourceCounter(it.id!!, resource.id!!, 0, 15) }
                }

            private val habitResources =
                habits.map { habit -> HabitResource(habit.id!!, 1) }

            private val habitTimeResource =
                listOf<HabitTime>()

            override fun getHabits(): List<Habit> = habits


            override fun getHabitTimeByHabitId(habitId: Long): List<HabitTime> =
                habitTimeResource.filter { it.habitId == habitId }


            override fun getResourceById(resourceId: Long): Resource? =
                resources.find { it.id == resourceId }

            override fun getResourceCounterByResourceId(
                resourceId: Long,
                habitId: Long
            ): ResourceCounter? =
                resourceCounters.find { it.resourceId == resourceId && it.habitId == habitId }


            override fun getHabitResourceByHabitId(habitId: Long) =
                habitResources.filter { it.habitId == habitId }
        }
    }
}