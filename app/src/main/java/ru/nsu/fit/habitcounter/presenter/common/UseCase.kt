package ru.nsu.fit.habitcounter.presenter.common

import ru.nsu.fit.habitcounter.callback.CallBack

abstract class UseCase<ResponseType : UseCase.ResponseValue, RequestType : UseCase.RequestValue> {
    interface ResponseValue
    interface RequestValue

    abstract var callBack: CallBack<ResponseType>
    abstract var requestValue: RequestType
    fun run() = execute(requestValue)
    protected abstract fun execute(x: RequestType)
}