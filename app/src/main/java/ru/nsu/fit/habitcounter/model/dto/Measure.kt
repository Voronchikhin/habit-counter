package ru.nsu.fit.habitcounter.model.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Measure(
        val name: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}