package ru.nsu.fit.habitcounter.presenter.common

import android.os.Handler
import ru.nsu.fit.habitcounter.callback.CallBack
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object UseCaseSchedulerImpl : UseCaseScheduler {
    private const val POOL_SIZE = 4
    private val handler = Handler()
    val threadPoolExecutor: ExecutorService = Executors.newFixedThreadPool(POOL_SIZE)
    override fun execute(runnable: Runnable) {
        threadPoolExecutor.submit(runnable)
    }

    override fun <T : UseCase.ResponseValue> onResponse(response: T, callBack: CallBack<T>) {
        handler.post{callBack.onSuccess(response)}
    }

    override fun <T : UseCase.ResponseValue> onError(callBack: CallBack<T>) {
        handler.post{callBack.onError()}
    }
}