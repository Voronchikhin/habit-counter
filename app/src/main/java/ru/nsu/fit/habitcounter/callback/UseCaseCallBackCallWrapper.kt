package ru.nsu.fit.habitcounter.callback

import ru.nsu.fit.habitcounter.presenter.common.UseCase
import ru.nsu.fit.habitcounter.presenter.common.UseCaseHandler

class UseCaseCallBackCallWrapper<ResponseType : UseCase.ResponseValue>
    (val callBack: CallBack<ResponseType>, private val handler: UseCaseHandler) :
    CallBack<ResponseType> {
    override fun onSuccess(response: ResponseType) {
        handler.notifyOnSuccess(response, callBack)
    }

    override fun onError() {
        handler.notifyOnError(callBack)
    }
}