package ru.nsu.fit.habitcounter.dao

import android.os.Build
import androidx.annotation.RequiresApi
import ru.nsu.fit.habitcounter.callback.GetCountersCallBack
import ru.nsu.fit.habitcounter.callback.GetHabitTimeCallBack
import ru.nsu.fit.habitcounter.callback.UpdateHabitCallBack
import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.model.ResourceAndCounter
import ru.nsu.fit.habitcounter.model.dto.HabitTime
import java.time.LocalDateTime

class HabitCounterRepositoryImpl(private val habitDataSource: HabitDataSource) :
    HabitCounterRepository {
    override fun getHabitCounters(getCountersCallBack: GetCountersCallBack) {
        val habits = habitDataSource.getHabits()

        try {
            val habitCounters = habits.map { habit ->
                HabitCounter(
                    habit,
                    habitDataSource.getHabitResourceByHabitId(habit.id!!)
                        .map {
                            ResourceAndCounter(
                                habitDataSource.getResourceById(it.resourceId)!!,
                                habitDataSource.getResourceCounterByResourceId(
                                    it.resourceId,
                                    it.habitId
                                )!!
                            )
                        }
                )
            }
            getCountersCallBack.onCountersLoaded(habitCounters)
        } catch (e: NullPointerException) {
            getCountersCallBack.onError()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun updateHabitCounter(habitCounter: HabitCounter, updateHabitCallBack: UpdateHabitCallBack) {
        habitDataSource.updateHabit(habitCounter.habit)
        habitDataSource.insertHabitTime(HabitTime().apply {
            this.habitId = habitCounter.habit.id!!
            this.time = LocalDateTime.now() })
        habitCounter.resourceCounters.forEach {
            habitDataSource.updateResource(it.resource)
            habitDataSource.updateResourceCounter(it.resourceCounter)
        }

        updateHabitCallBack.onAddHabit(habitCounter)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun getHabitTimeByHabitId(habitCounter: HabitCounter, getHabitTimeCallBack: GetHabitTimeCallBack) {
        val habitTimes = habitDataSource.getHabitTimeByHabitId(habitCounter.habit.id!!)

        getHabitTimeCallBack.onGetHabitTimes(habitTimes)
    }
}
