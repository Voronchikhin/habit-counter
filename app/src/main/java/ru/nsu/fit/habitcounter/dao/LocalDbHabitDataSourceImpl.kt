package ru.nsu.fit.habitcounter.dao

import android.content.Context
import ru.nsu.fit.habitcounter.dao.room.HabitDB
import ru.nsu.fit.habitcounter.model.dto.*

class LocalDbHabitDataSourceImpl(context: Context) : HabitDataSource {
    private val habitDB = HabitDB.getInstance(context)

    override fun updateResource(resource: Resource) {
        habitDB?.resourceDao?.update(resource)
    }

    override fun updateHabit(habit: Habit) {
        habitDB?.habitDao?.update(habit)
    }

    override fun updateHabitResource(habitResource: HabitResource) {
        habitDB?.habitResourceDao?.update(habitResource)
    }

    override fun updateResourceCounter(resourceCounter: ResourceCounter) {
        habitDB?.resourceCounterDao?.update(resourceCounter)
    }

    override fun insertHabitTime(habitTime: HabitTime) {
        habitDB?.habitTimeDao?.insert(habitTime)
    }

    override fun getHabitTimeByHabitId(habitId: Long): List<HabitTime> =
        habitDB?.habitTimeDao?.getByHabitId(habitId) ?: emptyList()

    override fun getHabitTimes(): List<HabitTime> =
        habitDB?.habitTimeDao?.getAll() ?: emptyList()

    override fun getHabits(): List<Habit> = habitDB?.habitDao?.getAll() ?: emptyList()

    override fun getResourceCounters(): List<ResourceCounter> =
        habitDB?.resourceCounterDao?.getAll() ?: emptyList()

    override fun getResources(): List<Resource> = habitDB?.resourceDao?.getAll() ?: emptyList()

    override fun getHabitResourceByHabitId(habitId: Long): List<HabitResource> =
        habitDB?.habitResourceDao?.getByHabitId(habitId) ?: emptyList()

    override fun getHabitResources(): List<HabitResource> =
        habitDB?.habitResourceDao?.getAll() ?: emptyList()

    override fun getResourceById(resourceId: Long): Resource? =
        habitDB?.resourceDao?.getById(resourceId)

    override fun getResourceCounterByResourceId(resourceId: Long, habitId: Long): ResourceCounter? =
        habitDB?.resourceCounterDao?.getByResourceId(resourceId)?.find { it.habitId == habitId }
}