package ru.nsu.fit.habitcounter.dao

import ru.nsu.fit.habitcounter.callback.GetCountersCallBack
import ru.nsu.fit.habitcounter.callback.GetHabitTimeCallBack
import ru.nsu.fit.habitcounter.callback.UpdateHabitCallBack
import ru.nsu.fit.habitcounter.model.HabitCounter


interface HabitCounterRepository {
    fun getHabitCounters(getCountersCallBack: GetCountersCallBack)
    fun updateHabitCounter(habitCounter: HabitCounter, updateHabitCallBack: UpdateHabitCallBack) // could update exists habit
    fun getHabitTimeByHabitId(habitCounter: HabitCounter, getHabitTimeCallBack: GetHabitTimeCallBack)
}


interface UpdateHabitCallBack {
    fun onAddHabit(addedCounter: HabitCounter)
    fun onError()
}