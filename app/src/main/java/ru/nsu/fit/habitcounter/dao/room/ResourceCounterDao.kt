package ru.nsu.fit.habitcounter.dao.room

import androidx.room.*
import ru.nsu.fit.habitcounter.model.dto.ResourceCounter

@Dao
interface ResourceCounterDao {
    @Query("SELECT * FROM RESOURCECOUNTER")
    fun getAll(): List<ResourceCounter>

    @Query("SELECT * FROM ResourceCounter WHERE resourceId = :resourceId")
    fun getByResourceId(resourceId: Long): List<ResourceCounter>

    @Insert
    fun insert(vararg resourceCounter: ResourceCounter)

    @Update
    fun update(vararg resourceCounter: ResourceCounter)

    @Delete
    fun delete(vararg resourceCounter: ResourceCounter)
}