package ru.nsu.fit.habitcounter.dao.room

import androidx.room.*
import ru.nsu.fit.habitcounter.model.dto.UserHabit

@Dao
interface UserHabitDao {
    @Query("SELECT * FROM UserHabit")
    fun getAll(): List<UserHabit>

    @Query("SELECT * FROM UserHabit WHERE userId = :userId")
    fun getByUserId(userId: Long): List<UserHabit>

    @Query("SELECT * FROM UserHabit WHERE habitId = :habitId")
    fun getByHabitId(habitId: Long): List<UserHabit>

    @Insert
    fun insert(vararg userHabit: UserHabit)

    @Update
    fun update(vararg userHabit: UserHabit)

    @Delete
    fun delete(vararg userHabit: UserHabit)
}