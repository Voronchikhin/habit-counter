package ru.nsu.fit.habitcounter.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import ru.nsu.fit.habitcounter.presenter.StatisticsPresenter
import ru.nsu.fit.habitcounter.presenter.common.UseCaseHandler
import ru.nsu.fit.habitcounter.view.base.SingleFragmentActivity


class StatisticsActivity : SingleFragmentActivity() {

    private lateinit var statisticsPresenter: StatisticsPresenter
    private lateinit var statisticsView: IStatisticsView

    override fun createFragment(): Fragment = StatisticsFragment()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        statisticsView = fragment as IStatisticsView
        statisticsPresenter = StatisticsPresenter(statisticsView, UseCaseHandler())
        statisticsView.presenter = statisticsPresenter
    }
}
