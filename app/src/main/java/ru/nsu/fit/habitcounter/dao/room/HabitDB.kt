package ru.nsu.fit.habitcounter.dao.room

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import ru.nsu.fit.habitcounter.dao.HabitDataSource
import ru.nsu.fit.habitcounter.model.dto.*
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*
import java.util.concurrent.Executors

@Database(entities = [Habit::class, HabitResource::class, Resource::class, ResourceCounter::class, HabitTime::class], version = 2)
@TypeConverters(Converters::class)
abstract class HabitDB : RoomDatabase() {
    abstract val habitDao: HabitDao
    abstract val resourceDao: ResourceDao
    abstract val resourceCounterDao: ResourceCounterDao
    abstract val habitResourceDao: HabitResourceDao
    abstract val habitTimeDao: HabitTimeDao

    companion object {
        private var INSTANCE: HabitDB? = null

        fun getInstance(context: Context): HabitDB? {
            if (INSTANCE == null) {
                synchronized(HabitDB::class) {
                    INSTANCE = createDb(context)
                }
            }
            return INSTANCE
        }

        private fun createDb(context: Context): HabitDB {
            return Room.databaseBuilder(
                context.applicationContext,
                HabitDB::class.java, "habit_counter.db"
            ).allowMainThreadQueries()
                .addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)

                        Executors.newSingleThreadExecutor().execute {
                            Thread.sleep(1000)
                            getInstance(context)?.resourceDao?.insert(*HabitDataSource.MEMORY_DATA_SOURCE.getResources().toTypedArray())
                            getInstance(context)?.habitDao?.insert(*HabitDataSource.MEMORY_DATA_SOURCE.getHabits().toTypedArray())
                            getInstance(context)?.resourceCounterDao?.insert(*HabitDataSource.MEMORY_DATA_SOURCE.getResourceCounters().toTypedArray())
                            getInstance(context)?.habitResourceDao?.insert(*HabitDataSource.MEMORY_DATA_SOURCE.getHabitResources().toTypedArray())
                            getInstance(context)?.habitTimeDao?.insert(*HabitDataSource.MEMORY_DATA_SOURCE.getHabitTimes().toTypedArray())

                        }
                    }
                })
                .build()
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}

class Converters {
    @RequiresApi(Build.VERSION_CODES.O)
    @TypeConverter
    fun fromTimestamp(value: Long?): LocalDateTime? {
        return value?.let { LocalDateTime.ofInstant(Instant.ofEpochMilli(value), ZoneOffset.UTC) }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @TypeConverter
    fun dateToTimestamp(date: LocalDateTime?): Long? {
        return date?.atZone(ZoneOffset.UTC)?.toInstant()?.toEpochMilli()
    }
}