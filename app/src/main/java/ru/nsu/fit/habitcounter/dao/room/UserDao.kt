package ru.nsu.fit.habitcounter.dao.room

import androidx.room.*
import ru.nsu.fit.habitcounter.model.dto.User

interface UserDao {
    @Query("SELECT * FROM USER")
    fun getAll(): List<User>

    @Query("SELECT * FROM USER WHERE name= :name")
    fun getByName(name: String): User

    @Query("SELECT * FROM USER WHERE id= :id")
    fun getById(id: Long): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg user: User)

    @Update
    fun update(vararg user: User)

    @Delete
    fun delete(vararg user: User)
}