package ru.nsu.fit.habitcounter.presenter


import ru.nsu.fit.habitcounter.callback.GetHabitTimeCallBack
import ru.nsu.fit.habitcounter.dao.HabitCounterRepository
import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.model.dto.HabitTime
import ru.nsu.fit.habitcounter.callback.CallBack
import ru.nsu.fit.habitcounter.presenter.common.UseCase

class GetStatisticsUseCase(private val habitCounterRepository: HabitCounterRepository) : UseCase<GetStatisticsResponse, GetStatisticsRequest>() {

    override lateinit var callBack: CallBack<GetStatisticsResponse>
    override lateinit var requestValue: GetStatisticsRequest

    override fun execute(x: GetStatisticsRequest) {
        val habitCounter = x.habitCounter
        habitCounterRepository.getHabitTimeByHabitId(habitCounter, object: GetHabitTimeCallBack {
            override fun onGetHabitTimes(habitTimes: List<HabitTime>) {
                callBack.onSuccess(GetStatisticsResponse(habitTimes.toList()))
            }
            override fun onError() {
                callBack.onError()
            }
        })
    }
}

class GetStatisticsResponse(val habitTimes: List<HabitTime>) : UseCase.ResponseValue

data class GetStatisticsRequest(val habitCounter: HabitCounter) : UseCase.RequestValue