package ru.nsu.fit.habitcounter.model.dto

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Resource(
    val name: String,
    val description : String
){
    @PrimaryKey(autoGenerate = true)
    var id : Long? = null
}