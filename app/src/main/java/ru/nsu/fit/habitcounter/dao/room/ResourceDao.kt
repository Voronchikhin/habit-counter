package ru.nsu.fit.habitcounter.dao.room

import androidx.room.*
import ru.nsu.fit.habitcounter.model.dto.Resource

@Dao
interface ResourceDao {
    @Query("SELECT * FROM RESOURCE")
    fun getAll(): List<Resource>

    @Query("SELECT * FROM RESOURCE WHERE name= :name")
    fun getByName(name: String): Resource?

    @Query("SELECT * FROM RESOURCE WHERE id= :id")
    fun getById(id: Long): Resource?

    @Insert
    fun insert(vararg resource: Resource)

    @Update
    fun update(vararg resource: Resource)

    @Delete
    fun delete(vararg resource: Resource)
}