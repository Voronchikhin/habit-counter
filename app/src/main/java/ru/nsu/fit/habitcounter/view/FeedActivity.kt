package ru.nsu.fit.habitcounter.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import ru.nsu.fit.habitcounter.dao.HabitCounterRepositoryImpl
import ru.nsu.fit.habitcounter.presenter.HabitPresenter
import ru.nsu.fit.habitcounter.presenter.common.UseCaseHandler
import ru.nsu.fit.habitcounter.dao.LocalDbHabitDataSourceImpl
import ru.nsu.fit.habitcounter.view.base.SingleFragmentActivity


class FeedActivity : SingleFragmentActivity() {
    private lateinit var habitPresenter: HabitPresenter
    private lateinit var habitView: IHabitView

    override fun createFragment(): Fragment = FeedFragment()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        habitView = fragment as IHabitView
        habitPresenter = HabitPresenter(
            habitView,
            UseCaseHandler(),
            HabitCounterRepositoryImpl(LocalDbHabitDataSourceImpl(applicationContext))
        )
        habitView.presenter = habitPresenter
    }

}
