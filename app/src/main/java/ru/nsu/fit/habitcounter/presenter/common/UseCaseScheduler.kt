package ru.nsu.fit.habitcounter.presenter.common

import ru.nsu.fit.habitcounter.callback.CallBack

interface UseCaseScheduler {
    fun execute(runnable: Runnable)

    fun <T : UseCase.ResponseValue> onResponse(response :  T, callBack : CallBack<T>)
    fun <T : UseCase.ResponseValue> onError(callBack: CallBack<T>)
}