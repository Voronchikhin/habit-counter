package ru.nsu.fit.habitcounter.model

import ru.nsu.fit.habitcounter.model.dto.Resource
import ru.nsu.fit.habitcounter.model.dto.ResourceCounter

data class ResourceAndCounter (
    val resource: Resource,
    val resourceCounter: ResourceCounter
)