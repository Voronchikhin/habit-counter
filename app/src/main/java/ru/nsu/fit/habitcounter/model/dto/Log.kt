package ru.nsu.fit.habitcounter.model.dto

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Date

@Entity
data class Log(
        val userId: Long,
        val habitId: Long,
        val time: Date
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}