package ru.nsu.fit.habitcounter.presenter.common

interface Presenter {
    fun start()
}