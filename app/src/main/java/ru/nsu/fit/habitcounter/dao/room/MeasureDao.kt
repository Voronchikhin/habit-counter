package ru.nsu.fit.habitcounter.dao.room

import androidx.room.*
import ru.nsu.fit.habitcounter.model.dto.Measure

interface MeasureDao {
    @Query("SELECT * FROM MEASURE")
    fun getAll(): List<Measure>

    @Query("SELECT * FROM MEASURE WHERE name= :name")
    fun getByName(name: String): Measure

    @Query("SELECT * FROM MEASURE WHERE id= :id")
    fun getById(id: Long): Measure

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg measure: Measure)

    @Update
    fun update(vararg measure: Measure)

    @Delete
    fun delete(vararg measure: Measure)
}