package ru.nsu.fit.habitcounter.presenter

import io.mockk.*
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import ru.nsu.fit.habitcounter.dao.HabitCounterRepository
import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.callback.CallBack
import ru.nsu.fit.habitcounter.callback.GetCountersCallBack
import ru.nsu.fit.habitcounter.presenter.common.UseCase
import ru.nsu.fit.habitcounter.presenter.common.UseCaseHandler
import ru.nsu.fit.habitcounter.presenter.common.View
import ru.nsu.fit.habitcounter.view.IHabitView

class HabitPresenterTest {

    var view: View<HabitPresenter> = mockk(relaxed = true)
    var useCaseHandler = UseCaseHandler()
    var habitPresenter = HabitPresenter(view, useCaseHandler, mockk())

    @Before
    fun initPresenter() {
        view = mockk()
        useCaseHandler = UseCaseHandler()
        habitPresenter
    }

    @Test
    fun simpleTest() {
        val view: View<HabitPresenter> = mockk(relaxed = true)
        val useCaseHandler = UseCaseHandler()
        val habitPresenter =
            HabitPresenter(view, useCaseHandler, mockk())

        habitPresenter.start()
    }

    @Test
    fun testStartBehavior() {
        val view: View<HabitPresenter> = mockk(relaxed = true)
        val useCaseHandler = spyk(UseCaseHandler())
        val habitPresenter = HabitPresenter(view, useCaseHandler, mockk())
        habitPresenter.start()
        verify {
            useCaseHandler.execute(any<UseCase<HabitResponse, HabitRequest>>(), any(), any())
        }
    }

    @Test
    fun testShowLoaded() {
        val view: IHabitView = mockk(relaxed = true)
        val useCaseHandler = spyk(UseCaseHandler())
        val repository: HabitCounterRepository = mockk()

        val habitPresenter = HabitPresenter(view, useCaseHandler, repository)
        val onLoadCallBack = slot<GetCountersCallBack>()
        val useCaseSlot = slot<CallBack<HabitResponse>>()
        every {
            repository.getHabitCounters(capture(onLoadCallBack))
        }.answers {
            onLoadCallBack.captured.onCountersLoaded(mockk())
        }
        every {
            useCaseHandler.execute(
                any<UseCase<HabitResponse, HabitRequest>>(),
                any(),
                capture(useCaseSlot)
            )
        }.answers {
            useCaseSlot.captured.onSuccess(mockk(relaxed = true))
        }
        habitPresenter.start()

        verify {
            view.showHabits(any())
        }
    }

    @Test
    @Ignore("fuck gitlab")
    //@Ignore("CI fails on it test, but locally OK")
    fun testDoCount() {
        val view: IHabitView = mockk(relaxed = true)
        val useCaseHandler = spyk(UseCaseHandler())
        val repository: HabitCounterRepository = mockk()

        val habitPresenter = HabitPresenter(view, useCaseHandler, repository)

        val habitCounter = mockk<HabitCounter>(relaxed = true)
        habitPresenter.doCount(habitCounter)
        verify {
            habitCounter.doCount()
        }
        verify {
            repository.updateHabitCounter(habitCounter, any())
        }
    }

    @Test
    fun testUpdateViewAfterCount() {
        val view: IHabitView = mockk(relaxed = true)
        val useCaseHandler: UseCaseHandler = mockk()
        val repository: HabitCounterRepository = mockk()

        val habitPresenter = HabitPresenter(view, useCaseHandler, repository)
        val doCountCallBack = slot<CallBack<DoCountResponse>>()
        val habitCounter = mockk<HabitCounter>(relaxed = true)
        every {
            useCaseHandler.execute(
                any<DoCountUseCase>(),
                DoCountRequest(habitCounter),
                capture(doCountCallBack)
            )
        }.answers {
            doCountCallBack.captured.onSuccess(DoCountResponse(habitCounter))
        }
        habitPresenter.doCount(habitCounter)
        verify {
            view.updateHabit(any())
        }
    }

}