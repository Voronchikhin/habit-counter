package ru.nsu.fit.habitcounter.presenter

import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.hamcrest.core.IsEqual
import org.junit.Assert.assertThat
import org.junit.Test
import ru.nsu.fit.habitcounter.callback.CallBack
import ru.nsu.fit.habitcounter.callback.GetCountersCallBack
import ru.nsu.fit.habitcounter.dao.HabitCounterRepository
import ru.nsu.fit.habitcounter.dao.HabitCounterRepositoryImpl
import ru.nsu.fit.habitcounter.dao.HabitDataSource
import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.model.ResourceAndCounter

class LoadHabitCountersTest {

    @Test
    fun simpleTest() {
        val habitDataSource = HabitDataSource.MEMORY_DATA_SOURCE
        val repository = HabitCounterRepositoryImpl(habitDataSource)
        val loadHabitCounters = LoadHabitCountersUseCase(repository)
        val habits = habitDataSource.getHabits();


        val habitCounters = habits.map { habit ->
            HabitCounter(
                habit,
                habitDataSource.getHabitResourceByHabitId(habit.id!!)
                    .map {
                        ResourceAndCounter(
                            habitDataSource.getResourceById(it.resourceId)!!,
                            habitDataSource.getResourceCounterByResourceId(
                                it.resourceId,
                                habit.id!!
                            )!!
                        )
                    }
            )
        }

        loadHabitCounters.callBack = object :
            CallBack<HabitResponse> {
            override fun onSuccess(response: HabitResponse) {
                assertThat(habitCounters, IsEqual(response.list))
            }

            override fun onError() {
                assert(false)
            }
        }
    }

    @Test
    fun testError() {
        val repository: HabitCounterRepository = mockk()
        val myCallBack: CallBack<HabitResponse> = mockk(relaxed = true)
        val loadCallBack = slot<GetCountersCallBack>()
        val loadHabitCounters = LoadHabitCountersUseCase(repository).apply {
            callBack = myCallBack
            requestValue = HabitRequest()
        }

        every {
            repository.getHabitCounters(capture(loadCallBack))
        }.answers {
            loadCallBack.captured.onError()
        }
        loadHabitCounters.run()
        verify {
            myCallBack.onError()
        }
    }
}