package ru.nsu.fit.habitcounter.model

import org.hamcrest.core.IsEqual
import org.junit.Assert.assertThat
import org.junit.Test
import ru.nsu.fit.habitcounter.model.dto.Habit
import ru.nsu.fit.habitcounter.model.dto.Resource
import ru.nsu.fit.habitcounter.model.dto.ResourceCounter


class HabitCounterTest {
    @Test
    fun testDoCount() {
        val habit = Habit("habit name", "do something bad")
            .apply { id = 0 }
        val resource = Resource("time", "one of the most valuable resources")
            .apply { id = 0 }
        val resourceCounter = ResourceCounter(habit.id!!, resource.id!!, 0, 15)
        val habitCounter =
            HabitCounter(habit, listOf(ResourceAndCounter(resource, resourceCounter)))
        habitCounter.doCount()
        assertThat(15, IsEqual(resourceCounter.count))
    }

    @Test
    fun testMoreThenOneCounters() {
        val habit = Habit("habit name", "do something bad")
            .apply { id = 0 }
        val resource = Resource("time", "one of the most valuable resources")
            .apply { id = 0 }
        val resourceCounter1 = ResourceCounter(habit.id!!, resource.id!!, 0, 15)
        val resourceCounter2 = ResourceCounter(habit.id!!, resource.id!!, 0, 15)
        val habitCounter =
            HabitCounter(
                habit,
                listOf(
                    ResourceAndCounter(resource, resourceCounter1),
                    ResourceAndCounter(resource, resourceCounter2)
                )
            )
        habitCounter.doCount()
        assertThat(15, IsEqual(resourceCounter1.count))
        assertThat(15, IsEqual(resourceCounter2.count))
    }
}