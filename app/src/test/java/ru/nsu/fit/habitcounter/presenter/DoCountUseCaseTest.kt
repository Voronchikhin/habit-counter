package ru.nsu.fit.habitcounter.presenter

import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.Test
import ru.nsu.fit.habitcounter.dao.HabitCounterRepository
import ru.nsu.fit.habitcounter.model.HabitCounter
import ru.nsu.fit.habitcounter.callback.CallBack
import ru.nsu.fit.habitcounter.callback.UpdateHabitCallBack

class DoCountUseCaseTest {
    @Test
    fun simpleTest() {
        val repository: HabitCounterRepository = mockk(relaxed = true)
        val myCallBack: CallBack<DoCountResponse> = mockk(relaxed = true)
        val habitCounter: HabitCounter = mockk(relaxed = true)

        val doCountUseCase = DoCountUseCase(repository).apply {
            callBack = myCallBack
            requestValue = DoCountRequest(habitCounter)
        }
        doCountUseCase.run()

        verify(exactly = 1) {
            repository.updateHabitCounter(any(), any())
        }
        verify {
            myCallBack.onSuccess(any())
        }
        verify {
            habitCounter.doCount()
        }
    }

    @Test
    fun checkErrorHandling() {
        val repository: HabitCounterRepository = mockk()
        val myCallBack: CallBack<DoCountResponse> = mockk(relaxed = true)
        val habitCounter: HabitCounter = mockk(relaxed = true)
        val id = slot<UpdateHabitCallBack>()

        every {
            repository.updateHabitCounter(any(), capture(id))
        }.answers {
            let { id.captured.onError() }
        }
        val doCountUseCase = DoCountUseCase(repository).apply {
            callBack = myCallBack
            requestValue = DoCountRequest(habitCounter)
        }
        doCountUseCase.run()
        verify {
            myCallBack.onError()
        }
    }
}